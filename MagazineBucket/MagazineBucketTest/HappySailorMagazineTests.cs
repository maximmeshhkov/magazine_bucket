using System;
using System.Linq;
using FluentAssertions;
using MagazineBucket.Constants;
using MagazineBucket.MagazineHappySailor;
using MagazineBucket.MagazineHappySailor.Goods;
using NUnit.Framework;

namespace MagazineBucketTest
{
    [TestFixture]
    public class HappySailorMagazineTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void BucketPricesStacksCorrectlyTest()
        {
            var bucketOne = new HappySailorBucket<IHappySailorGood>(goods: new IHappySailorGood[] {new Banana(price: 10.10), new Apple(price:20.20)});
            var bucketTwo = new HappySailorBucket<IHappySailorGood>(goods: new IHappySailorGood[] { new Banana(price: 10.35), new Apple(price: 10.35)});

            (bucketOne + bucketTwo).GetGoods().Sum(x => x.Price).Should().Be(51d);
        }

        [Test]
        public void EmptyBucketsStacksCorrectlyTest()
        {
            var bucketOne = new HappySailorBucket<IHappySailorGood>();
            var bucketTwo = new HappySailorBucket<IHappySailorGood>();

            (bucketOne + bucketTwo).GetGoods().Sum(x => x.Price).Should().Be(0);
        }

        [Test]
        public void LinksToTheSameBucketsAreEqualsTest()
        {
            var bucketOne = new HappySailorBucket<IHappySailorGood>();
            var bucketTwo = bucketOne;

            (bucketOne == bucketTwo).Should().BeTrue();
        }

        [Test]
        public void LinksToTheDifferentBucketsAreNotEqualsTest()
        {
            var bucketOne = new HappySailorBucket<IHappySailorGood>();
            var bucketTwo = new HappySailorBucket<IHappySailorGood>();

            (bucketOne == bucketTwo).Should().BeFalse();
        }

        [Test]
        public void StackBucketsWithHighWeightProduceErrorTest()
        {
            var bucketOne = new HappySailorBucket<IHappySailorGood>(goods: new BigFruitForTesting());
            var bucketTwo = new HappySailorBucket<IHappySailorGood>(goods: new BigFruitForTesting());

            Assert.Throws<ArithmeticException>(() =>
            {
                // ReSharper disable once UnusedVariable
                var result = (bucketOne + bucketTwo);
            });

        }

        [Test]
        public void PutGoodsAtBucketAppliesCorrectlyTest()
        {
            var bucket = new HappySailorBucket<IHappySailorGood>(goods: new Banana());
            bucket.Put(new Apple(), new Banana());
            bucket.GetGoods().Should().HaveCount(3);
        }

        [Test]
        public void RemoveGoodsFromBucketAppliesCorrectlyTest()
        {
            var good = new Banana(price: 10);
            var bucket = new HappySailorBucket<IHappySailorGood>(goods: good);
            bucket.Pull(good);
            bucket.GetGoods().Should().HaveCount(0);
        }
    }

    public class BigFruitForTesting : IHappySailorGood
    {
        public string Name => nameof(BigFruitForTesting);
        public double Weight => GoodsPrices.HappySailorBucketWeight;
        public string Category { get; set; }
        public double Price { get; set; }
    }
}