using System;
using System.Collections.Generic;
using System.Linq;
using MagazineBucket.MagazineHappySailor;
using MagazineBucket.MagazineHappySailor.Goods;

namespace MagazineBucket
{
    public class Program
    {
        // ReSharper disable once UnusedParameter.Local
        private static void Main(string[] args)
        {
            var bucketOne = new HappySailorBucket<IHappySailorGood>();

            var tmp = new List<IHappySailorGood>
            {
                new Banana(),
                new Banana(price: 10.15),
                new Banana(price: 8.45),
                new Apple(price: 6.99),
                new Apple(),
                new Apple(price: 3.22)
            };

            bucketOne.Put(tmp.ToArray());

            Console.WriteLine("Here is example of customer bucket with few goods.");
            Console.WriteLine(bucketOne);
            Console.WriteLine();
            Console.WriteLine($"Total cost of bucket for now is {bucketOne.CalculateBucketPrice()}{HappySailorBucket<IHappySailorGood>.Currency}");
            Console.WriteLine();

            Console.WriteLine($"Remove one element with price 10.15{HappySailorBucket<IHappySailorGood>.Currency}.");
            bucketOne.Pull(tmp.First(x => Math.Abs(x.Price - 10.15) < 0.01));
            Console.WriteLine(bucketOne);
            Console.WriteLine();
            Console.WriteLine($"Total cost of bucket after removal one element {bucketOne.CalculateBucketPrice()}{HappySailorBucket<IHappySailorGood>.Currency}");
            Console.WriteLine();

            Console.WriteLine($"We can value(good) from bucket as for array: {bucketOne[1].ToString()}");
            Console.WriteLine();

            Console.WriteLine("Stack two buckets to one.");
            var bucketTwo = new HappySailorBucket<IHappySailorGood>(goods: new IHappySailorGood[] { new Banana(), new Banana(), new Banana() });
            Console.WriteLine(bucketOne + bucketTwo);
            Console.WriteLine();

            var bucketX = new HappySailorBucket<IHappySailorGood>();
            var bucketY = new HappySailorBucket<IHappySailorGood>();

            Console.WriteLine($"Bucket {nameof(bucketX)} the same as {nameof(bucketY)} ==> {bucketY == bucketX}");
            bucketY = bucketX;
            Console.WriteLine("Directing links and one more check ...");
            Console.WriteLine($"Bucket {nameof(bucketX)} the same as {nameof(bucketY)} ==> {bucketY == bucketX}");
            Console.WriteLine();

            Console.WriteLine("We can sum bucket and items from magazine, and receive the same bucket with +1 item");
            var theSameBucket = bucketOne + new Banana();
            Console.WriteLine($"Bucket {nameof(bucketOne)} the same as {nameof(theSameBucket)} ==> {bucketOne == theSameBucket}");
            Console.WriteLine();

            Console.WriteLine("We can replace owner with extension method");
            Console.WriteLine($"Old value is - {theSameBucket.OwnerName}; After replace - {theSameBucket.ReplaceOwner("New Custom Owner Name")}");
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
