﻿namespace MagazineBucket
{
    /// <summary>
    /// Abstract entity, needed to chain it with buckets and goods;
    /// </summary>
    public abstract class Magazine
    {
        public abstract string Name { get; }
        public abstract string Location { get; }
    }
}
