﻿namespace MagazineBucket.Constants
{
    public class GoodsPrices
    {
        public const double BananaPrice = 23.20d;
        public const double ApplePrice = 23.20d;

        public const string FruitCategory = "Fruit";

        //TODO: this is poor place for this constants, it should be moved to another class
        public const double HappySailorBucketWeight = 25d;
        public const string DefaultOwnerName = "Default Owner";
    }
}
