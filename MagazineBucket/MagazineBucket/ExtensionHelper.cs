﻿using System.Linq;
using MagazineBucket.MagazineHappySailor;
using MagazineBucket.MagazineHappySailor.Goods;

namespace MagazineBucket
{
    public static class ExtensionHelper
    {
        public static double CalculateBucketPrice<TItem>(this TItem bucket) where TItem : HappySailorBucket<IHappySailorGood>
        {
            return bucket.GetGoods().Sum(good => good.Price);
        }

        public static string ReplaceOwner<TItem>(this TItem bucket, string owner) where TItem : HappySailorBucket<IHappySailorGood>
        {
            if (!string.IsNullOrEmpty(owner))
                return bucket.OwnerName = owner;
            return bucket.OwnerName;
        }
    }
}
