﻿namespace MagazineBucket.MagazineHappySailor
{
    /// <summary>
    /// Example of implementation magazine class;
    /// </summary>
    public class HappySailorMagazine : Magazine
    {
        public override string Name => nameof(HappySailorMagazine);
        public override string Location => "Moscow";
    }
}
