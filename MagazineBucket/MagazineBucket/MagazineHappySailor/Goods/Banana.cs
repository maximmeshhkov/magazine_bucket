﻿using System;
using MagazineBucket.Constants;

namespace MagazineBucket.MagazineHappySailor.Goods
{
    public sealed class Banana : IHappySailorGood
    {
        public  string Name => $"{nameof(Banana)}";
        public double Weight => new Random().NextDouble() * (0.250 - 0.50) + 0.50;
        public string Category { get; set; }
        public double Price { get; set; }

        public Banana(string category = GoodsPrices.FruitCategory, double price = GoodsPrices.BananaPrice)
        {
            Category = category;
            Price = price;
        }

        public override string ToString()
        {
	        return $"{Name} with {nameof(Weight)} {Weight:0.00}.";
        }
    }
}
