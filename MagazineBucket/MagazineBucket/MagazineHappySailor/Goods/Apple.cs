﻿using System;
using MagazineBucket.Constants;

namespace MagazineBucket.MagazineHappySailor.Goods
{
    public class Apple : IHappySailorGood
    {
        public string Name => nameof(Apple);
        public double Weight => new Random().NextDouble() * (0.75 - 0.10) + 0.10;
        public string Category { get; set; }
        public double Price { get; set; }

        public Apple(string category = GoodsPrices.FruitCategory, double price = GoodsPrices.ApplePrice)
        {
            Category = category;
            Price = price;
        }

        public override string ToString()
        {
	        return $"{Name} with {nameof(Weight)} {Weight:0.00}.";
        }
    }
}
