﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MagazineBucket.Constants;
using MagazineBucket.MagazineHappySailor.Goods;

namespace MagazineBucket.MagazineHappySailor
{
    /// <summary>
    /// Example of implementation bucket class;
    /// </summary>
    /// <remarks>
    /// Contain list of elements, that represent collection of goods;
    /// </remarks>
    public class HappySailorBucket<T> : MagazineBucket where T : IHappySailorGood
    {
        public const string Currency = "$";
        //this property added ONLY for test string extensions
        public string OwnerName { get; set; }
        //this set as private for testing purpose (indexer and etc.);
        //best option to move this into "MagazineBucket" class and set as abstract;
        private readonly List<IHappySailorGood> _goods;
        public override double BucketWeight => GoodsPrices.HappySailorBucketWeight;

        public HappySailorBucket(string owner = GoodsPrices.DefaultOwnerName, params IHappySailorGood[] goods)
        {
            OwnerName = owner;
            this._goods = new List<IHappySailorGood>(goods);
        }

        /// <summary>
        /// Stacks two buckets; If result weight is too much for bucket = throws an error;
        /// When you sum bucket + item from the magazine, you receive the same(!) bucket plus this item or an error if weight to high; 
        /// </summary>
        /// <param name="bucketOne">Represent a bucket entity one.</param>
        /// <param name="bucketTwo">Represent a bucket entity two.</param>
        public static HappySailorBucket<T> operator +(HappySailorBucket<T> bucketOne, HappySailorBucket<T> bucketTwo)
        {
            //TODO: we can call a manager or tms, because error is too strict for this case
            if (bucketOne._goods.Sum(x => x.Weight) + bucketTwo._goods.Sum(x => x.Weight) > GoodsPrices.HappySailorBucketWeight)
                throw new ArithmeticException("Weight summary of two buckets too high");
            return new HappySailorBucket<T>(goods: bucketOne._goods.Union(bucketTwo._goods).ToArray());
        }

        //NOT A CLEVER solution at all, because in one case we have a new bucket and for another one => the same;
        //not obvious;
        //implemented to show how overload works;
        public static HappySailorBucket<T> operator +(HappySailorBucket<T> bucketOne, T item)
        {
            //TODO: we can call a manager or tms, because error is too strict for this case
            if (bucketOne._goods.Sum(x => x.Weight) + item.Weight > GoodsPrices.HappySailorBucketWeight)
                throw new ArithmeticException("Weight summary of two buckets too high");
            bucketOne.Put(item);
            return bucketOne;
        }

        public static bool operator ==(HappySailorBucket<T> bucketOne, HappySailorBucket<T> bucketTwo)
        {
            if (ReferenceEquals(bucketOne, null))
                return false;
            if (ReferenceEquals(bucketTwo, null))
                return false;

            return ReferenceEquals(bucketOne, bucketTwo);
        }

        public static bool operator !=(HappySailorBucket<T> bucketOne, HappySailorBucket<T> bucketTwo)
        {
            return !(bucketOne == bucketTwo);
        }

        /// <summary>
        /// Put a good into collection;
        /// </summary>
        /// <param name="goods">Represent a instances of goods, that you wish to put into bucket.</param>
        public void Put(params IHappySailorGood[] goods)
        {
            this._goods.AddRange(goods);
        }

        /// <summary>
        /// Remove a good from collection;
        /// </summary>
        /// <param name="good">Represent a instances of goods, that you wish to remove from bucket.</param>
        public void Pull(IHappySailorGood good)
        {
            _goods.Remove(good);
        }

        public List<IHappySailorGood> GetGoods()
        {
            return _goods;
        }

        public IHappySailorGood this[int index]
        {
            get => _goods[index];

            set => _goods[index] = value;
        }

        public override string ToString()
        {
            var builder = new StringBuilder("Goods:\r\n");
            foreach (var good in _goods)
            {
                builder.Append($"{good.Name} for {good.Price}{Currency} \r\n");
            }

            return builder.ToString().TrimEnd('\r', '\n');
        }

        protected bool Equals(HappySailorBucket<T> other)
        {
            return Equals(_goods, other._goods);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((HappySailorBucket<T>)obj);
        }

        public override int GetHashCode()
        {
            return (_goods != null ? _goods.GetHashCode() : 0);
        }
    }
}
