﻿namespace MagazineBucket
{
    public abstract class MagazineBucket
    {
        public abstract double BucketWeight { get; }
    }
}
