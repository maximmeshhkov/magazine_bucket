﻿namespace MagazineBucket
{
    /// <summary>
    /// Abstract entity, which described element, that can present in bucket; 
    /// </summary>
    public interface IGood
    {
        string Name { get; }
        double Weight { get; }
        string Category { get; set; }
        double Price { get; set; }
    }
}
